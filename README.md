# Refactor Legacy Code

Assignment to view one version of a structure/program and apply our freshly acquired skills (SOLID-designs) to find flaws and try to correct them!

## How it works:
No complete functionality, purely a case of trying to apply SOLID on an example structure, 
analysing and trying to fix the SOLID mistakes we "may" find. 

Scenario1: Original with comments trying to identify wrongs.

Scenario2: Trying to fix/change applying SOLID.

## About: 
An assignment, breaking down a code and using our logic to correct it. 