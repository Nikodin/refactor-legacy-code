package no.noroff.nicholas.scenario2;

public interface AlarmThreshold {
    //Moved values here, can't be modified directly within the class to break it.
    double OFFSET = 16; //OFFSET as its own interface, could probably be better?
    double LowPressureThreshold = 17;
    double HighPressureThreshold = 21;

    }

