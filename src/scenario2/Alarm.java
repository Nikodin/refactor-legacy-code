package no.noroff.nicholas.scenario2;

public class Alarm implements AlarmThreshold {
    //Integrate threshold values in an interface "AlarmThreshold",
    // closes the values off in the interface, adaptive. - D (uncertain)

    //Sensor sensor = new Sensor(); - "moved" - S

    //Changed to private, only changeable in Alarm.
    private boolean alarmOn = false;

    //As private, always checking "test" sensor (any sensor we feed).
    //Less coupled through interface. - D
    private void check(test sensor){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }


    public boolean isAlarmOn(test sensor){
        check(sensor);
        return alarmOn;

    }
}
