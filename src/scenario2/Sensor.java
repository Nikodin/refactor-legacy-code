package no.noroff.nicholas.scenario2;

import java.util.Random;

public class Sensor implements AlarmThreshold, test {
    //OFFSET moved to interface: dunno if it's a good idea as it is tied with the other values, can be its own.

    //Interface "test", linked through check method.
    @Override
    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

    //Have the calculator as a separate function, obj. as it is always used?
    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
    }


}
