package no.noroff.nicholas.scenario1;

public class Alarm {

    //Integrate in an interface, closes the values off for modification.
    private final double LowPressureThreshold = 17;
    private final double HighPressureThreshold = 21;

    //Open closed. Tightly coupled.
    //Should be within the sensorclass, or main, and not here. Something is fishy here.
    //Single responsibility principle!
    Sensor sensor = new Sensor();

    boolean alarmOn = false; // Can be modified, should maybe be private?

    //is not called within the class, and always should be. - S
    public void check(){
        double psiPressureValue = sensor.popNextPressurePsiValue();

        if(psiPressureValue < LowPressureThreshold || HighPressureThreshold < psiPressureValue){
            alarmOn = true;
        }
    }

    public boolean isAlarmOn(){
        //check here!
        return alarmOn;
    }
}
