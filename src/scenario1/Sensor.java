package no.noroff.nicholas.scenario1;

import java.util.Random;

// Overkill comment: could be an abstract class where different sensors could be constructed.
public class Sensor  {
    //Extend through an interface with the other hardcoded values. Adaptive.
    public static final double OFFSET = 16;

// OPEN/CLOSED - create an interface, as it is shared by all sensors.
    public double popNextPressurePsiValue(){
        double pressureTelemetryValue;
        pressureTelemetryValue = samplePressure();
        return OFFSET + pressureTelemetryValue;
    }

    //Single responsibility, generates a random value.
    private double samplePressure() {
        Random basicRandomNumbersGenerator = new Random();
        double pressureTelemetryValue = 6 * basicRandomNumbersGenerator.nextDouble() * basicRandomNumbersGenerator.nextDouble();
        return pressureTelemetryValue;
        //Own class, single responsibility. Since it is generating a dummie, sensor value.
    }
}
